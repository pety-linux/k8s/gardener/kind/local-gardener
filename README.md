# Deploying Gardener locally


## Prerequisities

#### 1. VM with the following configuration
   - CentOS 7 VM 
   - 8 GB RAM
   - 4 x vCPU
   - HDD 100GB
   - Installed Packages: git,

#### 2. Docker Installed & Running
```
curl -fsSL https://get.docker.com/ | sh
systemctl enable docker --now
```
#### 3. Download and Install GO
```
wget https://dl.google.com/go/go1.17.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.17.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
go version
``` 

#### 4. Install [kind](https://kind.sigs.k8s.io/docs/user/quick-start/)
```
go install sigs.k8s.io/kind@v0.13.0
mv go/bin/kind /usr/local/bin/
```

#### 5. Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x kubectl
mv kubectl /usr/local/bin/
```

### 6. Test it
```
kind create cluster  --image kindest/node:v1.23.0
kind delete cluster
```

----
## Deploying Gardener locally
#### 1. Clone Gardener Repository
```
git clone https://github.com/gardener/gardener.git
cd gardener
```

#### 2. Setting up the KinD cluster (garden and seed)
```
make kind-up
cp /root/gardener/example/gardener-local/kind/kubeconfig .kube/config
kind get clusters
kubectl get no
```

#### 3. Setting up Gardener
```
make gardener-up
kubectl get seed local
```

#### 4. Creating the Shoot cluster
```
kubectl apply -f example/provider-local/shoot.yaml
kubectl -n garden-local get shoot local
```

#### 5. Accessing the Shoot cluster
```
cat <<EOF | sudo tee -a /etc/hosts

# Manually created to access local Gardener shoot clusters with names 'local' or 'e2e-default' in the 'garden-local' namespace.
# TODO: Remove this again when the shoot cluster access is no longer required.
127.0.0.1 api.local.local.external.local.gardener.cloud
127.0.0.1 api.local.local.internal.local.gardener.cloud
127.0.0.1 api.e2e-default.local.external.local.gardener.cloud
127.0.0.1 api.e2e-default.local.internal.local.gardener.cloud
EOF
```
```
kubectl -n garden-local get secret local.kubeconfig -o jsonpath={.data.kubeconfig} | base64 -d > /tmp/kubeconfig-shoot-local.yaml
kubectl --kubeconfig=/tmp/kubeconfig-shoot-local.yaml get nodes
```

#### 6. Deleting the Shoot cluster
```
cd gardener/
./hack/usage/delete shoot local garden-local
```

#### 4. Creating as Shoot cluster
```
make kind-down
```

## Detailed Steps:
https://gardener.cloud/docs/gardener/deployment/getting_started_locally/#setting-up-the-kind-cluster-garden-and-seed
